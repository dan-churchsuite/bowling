<?php
namespace App;

use PHPUnit\Framework\TestCase;

class BowlingTest extends TestCase {

	public function testGutterGame() {
		$calc = new Game;
		$score = $calc->score('-- -- -- -- -- -- -- -- -- --');

		$this->assertSame(0, $score);
	}

}