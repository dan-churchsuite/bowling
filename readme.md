# Welcome to the ChurchSuite Bowling Kata

To try and get us in the headspace of using Test Driven Development, we want you to have a go at building a bowling score calculator.
The rules are outlined below

## Bowling rules 101
A standard bowling game involves 10 turns, or "frames".
In each standard frame (the last frame is special - see below), the bowler gets up to two tries to knock down all the pins.
If on their first roll, they knock down all the pins (i.e.  a “strike”) their turn is over, and their score for the frame is ten plus a strike bonus (See below)
If in two tries they knock them all down (i.e a “spare”), their score for the frame is ten plus a spare bonus (see below).
If in two tries, they fail to knock them all down, their score for that frame is the grand total of pins knocked down.
The game score is the total of all frame scores.

## Bonuses
When a player scores a spare, the score from their next roll is doubled
When a player scores a strike, the scores from each of their next two rolls are doubled

## Final frame
If the player scores a spare or strike in the last (tenth) frame, they get to throw one or two more bonus balls, respectively. These bonus throws are taken as part of the same turn. If the bonus throws knock down all the pins, the process does not repeat: the bonus throws are only used to calculate the score of the final frame.

## Technical stuff
- Your Game class should have single publicly accessible function called score() (or something similar).
- the score() function will receive a string representing a ten frame game of bowling and return a integer representing the total score for the game
- The input string will have a format like this "45 31 -- X 5/ 22 X 5/ 34 25"
- Each digit in the input string represents the score of one roll
- The spaces in the string demarcate frames
- "-" represents a gutter ball (0 pins knocked down)
- "X" represents a strike
- "/" represents a spare


## Example scores
"-- -- -- -- -- -- -- -- -- --" (10 frames of miss and miss) = 10 frames * 0 = 0
"-1 -1 -1 -1 -1 -1 -1 -1 -1 -1" (10 frames of miss and 1) = 10 frames * 1 = 10
"X X X X X X X X X XXX" (9 frames of strike, and one final frame of strike and two bonus strikes) = 10 frames * 30 points = 300
"9- 9- 9- 9- 9- 9- 9- 9- 9- 9-" (10 frames of 9 and miss) = 10 frames * 9 points = 90
"5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5" (9 frames of 5 and spare, and a final frame of 5 and spare and bonus of 5) = 10 frames * 15 points = 150
"x 34 -- -- -- -- -- -- -- --" (One Strike, a frame of 3 and 4, followed by 8 frames of miss and miss) = (10 + (2 x 3) + (2 x 4)) = 24
"5/ 3- -- -- -- -- -- -- -- --" (One spare, a frame of 3 and miss, followed by 8 frames of miss and miss) = (10 + (2 x 3)) = 16


## resources
handy calc for checking what a score should be
https://www.bowlinggenius.com/

Another description of the problem
https://codingdojo.org/kata/Bowling/

Check github for solutions if you are mega stuck

## Clues and advice
Remember you are trying to work in bite sized chunks and only wrtiting enough code to make each new test green.

While you may have a pretty good idea where you want to end up, its important for the excersise to not jump the gun.
Dont be afraid to code hard code stuff or take shortcuts.

If you are struggling of thinking where to start, think of the simplest possible score and and work from there bit by bit.

Check github for solutions if you are mega stuck
